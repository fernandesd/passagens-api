<?php

use App\Http\Controllers\Api\V1\Admin\FlightController;
use App\Http\Controllers\Api\V1\AuthController;
use App\Http\Controllers\Api\V1\Site\AirportController;
use App\Http\Controllers\Api\V1\Site\BuyerController;
use App\Http\Controllers\Api\V1\Site\OrderController;
use App\Http\Controllers\Api\V1\Site\TicketController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::get('/ping', function () {
    return response()->json('pong');
});

Route::prefix('v1')->as('api.')->group(function () {
    Route::controller(AuthController::class)->prefix('user')->group(function () {
        Route::post('/login', 'loginUser')->name('login');

        Route::middleware('auth:sanctum')->group(function () {
            Route::post('/', 'createUser')->name('register');
            Route::post('/logout', 'logout')->name('logout');
        });
    });

    Route::get('airports', [AirportController::class, 'index'])->name('airport.index');
    Route::controller(OrderController::class)->prefix('orders')->as('order.')->group(function () {
        Route::post('/', 'store')->name('store');
        Route::put('{orderCode}', 'cancel')->name('cancel');
    });
    Route::controller(TicketController::class)->prefix('tickets')->as('ticket.')->group(function () {
        Route::get('/', 'index')->name('index');
        Route::get('{ticketCode}/voucher', 'getVoucher')->name('voucher');
        Route::get('{ticketCode}/baggage/voucher', 'getBaggageVoucher')->name('baggage.voucher');
    });
    Route::get('buyer/{buyerCPF}/tickets', [BuyerController::class, 'getTickets'])->name('buyer.tickets');

    Route::prefix('admin')->as('admin.')->middleware('auth:sanctum')->group(function () {
        Route::controller(FlightController::class)->prefix('flights')->as('flight.')->group(function () {
            Route::post('/', 'store')->name('store');
            Route::get('/', 'index')->name('index');
            Route::get('{flight}/passengers', 'getPassengers')->name('passengers');
        });
    });
});
