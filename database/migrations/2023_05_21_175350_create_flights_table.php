<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('flights', function (Blueprint $table) {
            $table->id();
            $table->string('code')->default(uniqid());
            $table->timestamp('depart_at');
            $table->enum('status', ['created', 'cancelled'])->default('created');

            $table->timestamps();
            $table->softDeletes();

            $table->foreignId('origin_airport_id')->constrained(
                table: 'airports', indexName: 'flights_origin_airport_id'
            );
            $table->foreignId('destination_airport_id')->constrained(
                table: 'airports', indexName: 'flights_destination_airport_id'
            );

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('flights');
    }
};
