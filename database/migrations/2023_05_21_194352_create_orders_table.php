<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->string('code')->default(uniqid());
            $table->id();
            $table->timestamps();
            $table->enum('status', ['cancelled', 'created'])->default('created');
            $table->integer('total');
            $table->softDeletes();

            $table->foreignId('buyer_id')->constrained();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('orders');
    }
};
