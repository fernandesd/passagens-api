<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->id();
            $table->string('code')->default(uniqid());
            $table->boolean('has_baggage')->default(false);
            $table->integer('total_price');

            $table->foreignId('passenger_id')->nullable()->constrained();
            $table->foreignId('flight_id')->constrained();
            $table->foreignId('flight_class_id')->constrained();

            $table->foreignId('buyer_id')->nullable()->constrained();
            $table->foreignId('order_id')->nullable()->constrained();

            $table->softDeletes();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('tickets');
    }
};
