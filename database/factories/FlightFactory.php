<?php

namespace Database\Factories;

use App\Models\Airport;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Flight>
 */
class FlightFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $airportOriginId = Airport::inRandomOrder()->limit(1)->get('id')->first()->id;

        return [
            'code' => uniqid(),
            'depart_at' => now()->addMonth(),
            'origin_airport_id' => $airportOriginId,
            'destination_airport_id' => function (array $attributes) {
                return Airport::where('city_id', '!=', $attributes['origin_airport_id'])
                    ->inRandomOrder()
                    ->limit(1)
                    ->get('id')
                    ->first()->id;
            },
        ];
    }
}
