<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Buyer>
 */
class BuyerFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'email' => $this->faker->unique()->email(),
            'name' => $this->faker->name(),
            'cpf' => $this->faker->cpf(false),
            'brithday' => $this->faker->dateTimeBetween('-70 years', '-20 years')->format('d/m/Y'),
        ];
    }
}
