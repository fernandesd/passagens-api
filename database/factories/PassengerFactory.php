<?php

namespace Database\Factories;

use App\Models\FlightClass;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Passenger>
 */
class PassengerFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'email' => $this->faker->unique()->email(),
            'name' => $this->faker->name(),
            'cpf' => $this->faker->cpf(false),
            'brithday' => $this->faker->dateTimeBetween('-70 years', '-20 years')->format('Y-m-d'),
            'flight_class_id' => FlightClass::inRandomOrder()->limit(1)->get('id')->first()->id,
        ];
    }
}
