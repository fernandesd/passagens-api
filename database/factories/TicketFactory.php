<?php

namespace Database\Factories;

use App\Models\Buyer;
use App\Models\Passenger;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Ticket>
 */
class TicketFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $passenger = Passenger::inRandomOrder()->limit(1)->with('flightClass')->get(['id', 'flight_class_id'])->first() ?? Passenger::factory()->hasFlightClass()->create();

        return [
            'code' => uniqid(),
            'has_baggage' => false,
            'total_price' => $this->faker->numberBetween(15000, 50000),
            'passenger_id' => $passenger->id,
            'flight_id' => $passenger->flightClass->flight_id,
            'flight_class_id' => $passenger->flight_class_id,
            'buyer_id' => Buyer::factory(),
            'order_id' => null,

        ];
    }
}
