<?php

namespace Database\Factories;

use App\Models\FlightClass;
use App\Models\Ticket;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Seat>
 */
class SeatFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $flightClass = FlightClass::inRandomOrder()->limit(1)->get(['id', 'capacity'])->first();
        $ticket = Ticket::inRandomOrder()->limit(1)->get(['id'])->first();

        return [
            'number' => $this->faker->unique(true)->randomNumber(2),
            'flight_class_id' => $flightClass->id,
            'ticket_id' => $ticket->id ?? Ticket::factory(),
            'code' => uniqid(),

        ];
    }
}
