<?php

namespace Database\Factories;

use App\Models\Flight;
use App\Models\FlightClass;
use App\Models\Ticket;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\FlightClass>
 */
class FlightClassFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $classes = [
            ['name' => 'economic', 'price' => '5000'],
            ['name' => 'business', 'price' => '10000'],
            ['name' => 'premium', 'price' => '20000'],
        ];
        $randomClass = $this->faker->randomElement($classes);

        return [
            'type' => $randomClass['name'],
            'capacity' => 30,
            'seat_price' => $randomClass['price'],
            'flight_id' => Flight::whereDoesntHave('classes', function (Builder $query) use ($randomClass) {
                $query->where('type', $randomClass['name']);
            })->inRandomOrder()->limit(1)->get('id')->first()->id ?? null,
        ];
    }

    /**
     * Configure the model factory.
     */
    public function configure(): static
    {
        return $this->afterMaking(function (FlightClass $flightClass) {
            // ...
        })->afterCreating(function (FlightClass $flightClass) {
            Ticket::factory($flightClass->capacity)
                ->hasSeat([
                    'flight_class_id' => $flightClass->id,
                    'number' => $this->faker->unique(true)->numberBetween(1, $flightClass->capacity),
                ])
                ->create([
                    'flight_class_id' => $flightClass->id,
                    'flight_id' => $flightClass->flight_id,
                ]);
        });
    }
}
