<?php

namespace Database\Seeders;

use App\Models\Airport;
use App\Models\City;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class AirportSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $airportsData = $this->generateAirportsData()->toArray();
        Airport::insert($airportsData);
    }

    public function generateAirportsData()
    {
        $airports = Storage::disk('data')->json('airports.json');
        $cities = City::get(['name', 'id']);

        return collect($airports)->map(fn ($airport) => [
            'iata_code' => $airport['iata'],
            'name' => $airport['cityAirportName'],
            'city_id' => $cities->firstWhere('name', $airport['city'])->id,
            'created_at' => now(),
        ]);
    }
}
