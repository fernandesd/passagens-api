<?php

namespace Database\Seeders;

use App\Connector\IBGEConnector;
use App\Models\State;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use Illuminate\Database\Seeder;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class CitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $states = State::get(['id', 'code', 'name']);
        $responses = (new IBGEConnector())->getStateCitiesPool($states);

        $citiesResponse = collect($responses)->map(function ($response) {
            if ($response->successful()) {
                return $response->json();
            }
        });

        $citiesResponse->each(function ($group) use ($states) {
            $citiesData = $this->generateCitiesData($group, $states);
            DB::table('cities')->insert($citiesData->toArray());
        });
    }

    /**
     * Recebe as cidades de um estado e monta o array para salvar no banco de dados
     *
     * @param  array  $cities - cidades de um determinado estado
     * @param  EloquentCollection  $states - todos os estados salvos no banco de dados
     * @return Collection - collection com os dados prontos para salvar no banco de dados
     */
    public function generateCitiesData(array $cities, EloquentCollection $states): Collection
    {
        return collect($cities)->sortBy(['nome', 'asc'])->map(function ($city) use ($states) {

            $cityUF = $city['microrregiao']['mesorregiao']['UF']['sigla'];

            return [
                'state_id' => $states->firstWhere('code', $cityUF)->id,
                'name' => $city['nome'],
                'uf' => $cityUF,
                'created_at' => now(),
            ];

        });
    }
}
