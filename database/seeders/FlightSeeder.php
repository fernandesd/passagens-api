<?php

namespace Database\Seeders;

use App\Models\Flight;
use App\Models\FlightClass;
use App\Models\Passenger;
use App\Models\Ticket;
use Illuminate\Database\Seeder;

class FlightSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $this->getSeed([
            'flight' => 3,
            'flightClass' => 1,
            'flightClassCapacity' => 15,
            'passenger' => 10,
        ]);

        $this->getSeed([
            'flight' => 2,
            'flightClass' => 1,
            'flightClassCapacity' => 5,
            'passenger' => 5,
        ]);
    }

    public function getSeed(array $config)
    {
        $flightClassSequences = $this->flightClassSequence($config);
        Flight::factory($config['flight'])
            ->has(
                FlightClass::factory($config['flightClass'])
                    ->sequence($flightClassSequences[0], $flightClassSequences[1], $flightClassSequences[2])
                    ->has(
                        Passenger::factory($config['passenger'])
                            ->has(
                                Ticket::factory(1)
                                    ->state(function (array $attributes, Passenger $passenger) {
                                        return [
                                            'has_baggage' => true,
                                            'flight_id' => $passenger->flightClass->flight_id,
                                        ];
                                    })
                                    ->hasBaggage(1)
                                    ->hasBuyer(1)
                                    ->hasSeat(1),
                                'ticket'
                            ),
                        'passengers'
                    ),
                'classes'
            )

            ->has(
                FlightClass::factory($config['flightClass'])
                    ->sequence($flightClassSequences[0], $flightClassSequences[1], $flightClassSequences[2])
                    ->has(
                        Passenger::factory($config['passenger'])
                            ->has(
                                Ticket::factory(1)
                                    ->state(function (array $attributes, Passenger $passenger) {
                                        return [
                                            'flight_id' => $passenger->flightClass->flight_id,
                                        ];
                                    })
                                    ->hasSeat(1)
                                    ->hasBuyer(1),
                                'ticket'),
                        'passengers'),
                'classes')
            ->create();

        Flight::factory(5)
            ->has(
                FlightClass::factory(1)
                    ->sequence($flightClassSequences[0], $flightClassSequences[1], $flightClassSequences[2])
                    ->has(
                        Ticket::factory($config['flightClassCapacity'])
                            ->state(function (array $attributes, FlightClass $flightClass) {
                                return [
                                    'buyer_id' => null,
                                    'passenger_id' => null,
                                    'flight_id' => $flightClass->flight_id,
                                ];
                            })
                            ->hasSeat(1),
                        'tickets'),
                'classes')
            ->create();
    }

    public function flightClassSequence(array $config): array
    {
        return [
            [
                'capacity' => $config['flightClassCapacity'],
                'type' => 'economic',
                'seat_price' => 5000,
            ],
            [
                'capacity' => $config['flightClassCapacity'],
                'type' => 'business',
                'seat_price' => 10000,
            ],
            [
                'capacity' => $config['flightClassCapacity'],
                'type' => 'premium',
                'seat_price' => 20000,
            ],

        ];
    }
}
