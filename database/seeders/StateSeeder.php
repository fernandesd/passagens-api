<?php

namespace Database\Seeders;

use App\Connector\IBGEConnector;
use App\Models\State;
use Illuminate\Database\Seeder;

class StateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $statesResponse = (new IBGEConnector())->getSync(endpoint: 'localidades/estados')->json();
        collect($statesResponse)->sortBy(['sigla', 'asc'])->each(function ($state) {
            $stateData = [
                'code' => $state['sigla'],
                'name' => $state['nome'],
            ];
            State::create($stateData);
        });
    }
}
