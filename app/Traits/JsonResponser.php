<?php

namespace App\Traits;

use Illuminate\Http\JsonResponse;

trait JsonResponser
{
    /**
     * Retorna uma resposta json.
     *
     * @param  array  $data <string, mixed>
     */
    protected function response(array $data): JsonResponse
    {
        return response()->json(data: $data['content'], status: $data['code']);
    }
}
