<?php

namespace App\Traits;

use Illuminate\Support\Facades\Validator as FacadesValidator;
use Illuminate\Validation\Validator;

trait HasValidator
{
    public function validator(array $requestData, array $rules): Validator
    {
        $validator = FacadesValidator::make($requestData, $rules);

        return $validator;
    }
}
