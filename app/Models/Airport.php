<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Airport extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'iata_code',
        'name',
    ];

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function flights()
    {
        return $this->hasMany(Flight::class);
    }
}
