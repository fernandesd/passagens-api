<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FlightClass extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'type', 'capacity', 'seat_price', 'flight_id',
    ];

    public function flight()
    {
        return $this->belongsTo(Flight::class);
    }

    public function seats()
    {
        return $this->hasMany(Seat::class);
    }

    public function passengers()
    {
        return $this->hasMany(Passenger::class);
    }

    public function tickets()
    {
        return $this->hasMany(Ticket::class);
    }
}
