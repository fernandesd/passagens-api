<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Passenger extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'email', 'name', 'cpf', 'brithday', 'flight_class_id',
    ];

    public function flightClass()
    {
        return $this->belongsTo(FlightClass::class);
    }

    public function ticket()
    {
        return $this->hasOne(Ticket::class);
    }
}
