<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Flight extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'code',
        'depart_at',
        'origin_airport_id',
        'destination_airport_id',
    ];

    public function origin()
    {
        return $this->belongsTo(Airport::class, 'origin_airport_id')->withDefault();
    }

    public function destination()
    {
        return $this->belongsTo(Airport::class, 'destination_airport_id')->withDefault();
    }

    public function classes()
    {
        return $this->hasMany(FlightClass::class);
    }

    public function tickets()
    {
        return $this->hasMany(Ticket::class);
    }
}
