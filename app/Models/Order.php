<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'buyer_id',
        'status',
        'total',
        'code',
    ];

    public function buyer()
    {
        return $this->belongsTo(Buyer::class);
    }

    public function tickets()
    {
        return $this->hasMany(Ticket::class);
    }
}
