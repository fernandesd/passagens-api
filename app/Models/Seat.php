<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Seat extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'number', 'flight_class_id', 'code',
    ];

    public function flightClass()
    {
        return $this->belongsTo(FlightClass::class);
    }

    public function ticket()
    {
        return $this->belongsTo(Ticket::class);
    }
}
