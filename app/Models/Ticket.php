<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ticket extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'code',
        'has_baggage',
        'total_price',
        'passenger_id',
        'flight_id',
        'buyer_id',
        'order_id',
        'flight_class_id',
    ];

    public function passenger()
    {
        return $this->belongsTo(Passenger::class);
    }

    public function flight()
    {
        return $this->belongsTo(Flight::class);
    }

    public function flightClass()
    {
        return $this->belongsTo(FlightClass::class);
    }

    public function baggage()
    {
        return $this->hasOne(Baggage::class);
    }

    public function seat()
    {
        return $this->hasOne(Seat::class);
    }

    public function buyer()
    {
        return $this->belongsTo(Buyer::class);
    }

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function scopeAvailableToBuy(Builder $query)
    {
        $query->doesntHave('buyer')->whereRelation('flight', 'depart_at', '>=', now());
    }
}
