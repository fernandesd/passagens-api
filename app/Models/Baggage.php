<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Baggage extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'ticket_id', 'code',
    ];

    public function ticket()
    {
        return $this->belongsTo(Ticket::class);
    }
}
