<?php

namespace App\Services\Api\V1\Site;

use App\Http\Resources\Api\V1\TicketBaggageVoucherResource;
use App\Http\Resources\Api\V1\TicketResource;
use App\Models\Ticket;
use App\Repository\Api\V1\Site\TicketRepository;
use App\Traits\HasValidator;
use Illuminate\Support\Carbon;

class TicketService
{
    use HasValidator;

    private $repository;

    public function __construct(TicketRepository $repository)
    {
        $this->repository = $repository;

    }

    /**
     * Lista as  passagens disponíveis podendo filtar por origem[IATA], destino[IATA] e data de partida[yyyy-mm-dd]
     *
     * @param  array  $queryData - dados para a filtragem*
     */
    public function listAll(array $request): array
    {
        if ($request) {
            $rules = [
                'departAt' => 'date-format:Y-m-d|required',
                'flightOrigin' => 'nullable|size:3',
                'flightDestination' => 'nullable|size:3'];

            $validator = $this->validator($request, $rules);
            if ($validator->fails()) {
                return [
                    'code' => 422,
                    'content' => $validator->errors(),
                ];
            }
            $tickets = $this->repository->filterFlightTicketsByData($validator->validated());

        } else {
            $tickets = $this->repository->getAll();
        }

        $ticketsData = TicketResource::collection($tickets);

        return [
            'code' => 200,
            'content' => $ticketsData,
        ];
    }

    /**
     * Gera o voucher de bagagem do ticket
     */
    public function getBaggageVoucher(string $ticketCode): array
    {
        try {
            $ticket = $this->repository->getTicketWithRelationsByCode($ticketCode);
        } catch (\Throwable $th) {
            return [
                'code' => 404,
                'content' => 'Recurso não encontrado',
            ];
        }

        if (! $this->isAbleToGetVoucher($ticket)) {
            return [
                'code' => 422,
                'content' => 'O prazo máximo para emitir o voucher é 5 horas antes do voo',
            ];
        }
        $ticketData = new TicketBaggageVoucherResource($ticket);

        return [
            'code' => 200,
            'content' => $ticketData,
        ];
    }

    /**
     * Gera o voucher do ticket
     */
    public function getVoucher(string $ticketCode): array
    {
        try {
            $ticket = $this->repository->getTicketWithRelationsByCode($ticketCode);
        } catch (\Throwable $th) {
            return [
                'code' => 404,
                'content' => 'Recurso não encontrado',
            ];
        }

        if (! $this->isAbleToGetVoucher($ticket)) {
            return [
                'code' => 422,
                'content' => 'O prazo máximo para emitir o voucher é 5 horas antes do voo',
            ];
        }
        $ticketData = new TicketResource($ticket);

        return [
            'code' => 200,
            'content' => $ticketData,
        ];
    }

    /**
     * verifica se há 5 horas para o voo
     */
    public function isAbleToGetVoucher(Ticket $ticket): bool
    {
        return ! Carbon::create($ticket->flight->depart_at)->subHours(5) <= now();
    }
}
