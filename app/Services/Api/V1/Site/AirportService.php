<?php

namespace App\Services\Api\V1\Site;

use App\Http\Resources\Api\V1\AirportCollection;
use App\Repository\Api\V1\Site\AirportRepository;

class AirportService
{
    private $repository;

    public function __construct(AirportRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Lista os aeroportos
     * retorna a lista com paginação
     */
    public function listAll(): array
    {
        $airports = $this->repository->getAll();
        $airportsData = new AirportCollection($airports);

        return [
            'code' => 200,
            'content' => $airportsData,
        ];
    }
}
