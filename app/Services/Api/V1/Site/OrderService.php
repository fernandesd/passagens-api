<?php

namespace App\Services\Api\V1\Site;

use App\Http\Resources\Api\V1\OrderResource;
use App\Models\Ticket;
use App\Repository\Api\V1\Site\BuyerRepository;
use App\Repository\Api\V1\Site\OrderRepository;
use App\Repository\Api\V1\Site\PassengerRepository;
use App\Repository\Api\V1\Site\TicketRepository;
use App\Traits\HasValidator;
use Illuminate\Support\Carbon;

class OrderService
{
    use HasValidator;

    private $ticketRepository;

    private $buyerRepository;

    private $passengerRepository;

    private $orderRepository;

    public function __construct(TicketRepository $ticketRepository, BuyerRepository $buyerRepository, PassengerRepository $passengerRepository, OrderRepository $orderRepository)
    {
        $this->ticketRepository = $ticketRepository;
        $this->buyerRepository = $buyerRepository;
        $this->passengerRepository = $passengerRepository;
        $this->orderRepository = $orderRepository;
    }

    /**
     * Realiza a compra das passagens
     */
    public function buy(array $buyData): array
    {
        $tickets = $this->ticketRepository->getTicketsWithRelationsByFlightData($buyData['flight'])->take($buyData['passengersCount']);
        $ticketIds = $tickets->pluck('id');

        $this->passengerRepository->createMany($buyData['passengers']);

        $buyer = $this->buyerRepository->new($buyData['buyer']);

        collect($buyData['passengers'])->each(function ($passengerData) use ($ticketIds) {
            $this->ticketRepository->attachPassengerIdOnSelectedTickets($passengerData, $ticketIds->pop());
        });

        $order = $this->ticketRepository->attachOrderIdOnSelectedTickets($buyer, $tickets->pluck('id')->toArray());

        $orderData = new OrderResource($order);

        return [
            'code' => 201,
            'content' => $orderData,
        ];
    }

    /**
     * verifica se há 5 horas para o voo
     */
    public function isAbleToGetVoucher(Ticket $ticket): bool
    {
        return ! Carbon::create($ticket->flight->depart_at)->subHours(5) <= now();
    }

     /**
      * Cancela um pedido pelo codigo
      */
     public function cancel($orderCode)
     {
         try {
             $order = $this->orderRepository->findByCode($orderCode);
         } catch (\Throwable $th) {
             return [
                 'code' => 404,
                 'content' => 'Recurso não encontrado',
             ];
         }

         $cancelledOrder = $this->orderRepository->updateToCancelOrder($order);

         $orderData = new OrderResource($cancelledOrder);

         return [
             'code' => 202,
             'content' => $orderData,
         ];
     }
}
