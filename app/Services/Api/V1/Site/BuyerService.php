<?php

namespace App\Services\Api\V1\Site;

use App\Http\Resources\Api\V1\TicketResource;
use App\Repository\Api\V1\Site\BuyerRepository;
use Illuminate\Support\Str;

class BuyerService
{
    private $repository;

    public function __construct(BuyerRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Obtém as passagens compradas através do cpf
     *
     * @param $buyercpf - cpf do comprador
     */
    public function getTickets(string $buyercpf): array
    {
        try {
            $tickets = $this->repository->getTickets(Str::remove(['.', '-', ' '], $buyercpf));
        } catch (\Throwable $th) {
            return [
                'code' => 404,
                'content' => 'Recurso não encontrado',
            ];
        }

        $ticketsData = TicketResource::collection($tickets);

        return [
            'code' => 200,
            'content' => $ticketsData,
        ];
    }
}
