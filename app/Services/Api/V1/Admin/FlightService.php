<?php

namespace App\Services\Api\V1\Admin;

use App\Http\Resources\Api\V1\FlightClassResource;
use App\Http\Resources\Api\V1\FlightCollection;
use App\Http\Resources\Api\V1\FlightResource;
use App\Repository\Api\V1\Admin\FlightRepository;

class FlightService
{
    private $repository;

    public function __construct(FlightRepository $repository)
    {
        $this->repository = $repository;
    }

     /**
      * Lista Todos os voos
      */
     public function listAll()
     {
         $flights = $this->repository->getAll();
         $flightsData = new FlightCollection($flights);

         return [
             'code' => 200,
             'content' => $flightsData,
         ];
     }

    /**
     * Lista passageiros de um voo
     */
    public function getPassengers(string $flightId)
    {
        try {
            $passengers = $this->repository->getPassengers($flightId);
        } catch (\Throwable $th) {
            return [
                'code' => 404,
                'content' => 'Recurso não encontrado',
            ];
        }

        $passengersData = FlightClassResource::collection($passengers);

        return [
            'code' => 200,
            'content' => $passengersData,
        ];
    }

    /**
     * Cria um novo voo
     */
    public function store(array $flightData)
    {
        $airports = $this->repository->getFlightAirports([$flightData['origin'], $flightData['destination']]);
        if ($airports->count() != 2) {
            return [
                'code' => 404,
                'content' => 'Os aeroportos não podem ser da mesma cidade',
            ];
        }

        $flightData['origin_airport_id'] = $airports->where('iata_code', $flightData['origin'])->first()->id;
        $flightData['destination_airport_id'] = $airports->where('iata_code', $flightData['destination'])->first()->id;

        $createdFlight = $this->repository->createFlight($flightData);
        $createdFlightData = new FlightResource($createdFlight);

        return [
            'code' => 201,
            'content' => $createdFlightData,
        ];
    }
}
