<?php

namespace App\Http\Controllers\Api\V1\Site;

use App\Http\Controllers\Controller;
use App\Services\Api\V1\Site\TicketService;
use Illuminate\Http\Request;

class TicketController extends Controller
{
    private $service;

    public function __construct(TicketService $service)
    {
        $this->service = $service;
    }

    public function index(Request $request)
    {
        $tickets = $this->service->listAll($request->query());

        return $this->response($tickets);
    }

    public function getBaggageVoucher(string $ticketCode)
    {
        $baggageVoucher = $this->service->getBaggageVoucher($ticketCode);

        return $this->response($baggageVoucher);
    }

    public function getVoucher(string $ticketCode)
    {
        $voucher = $this->service->getVoucher($ticketCode);

        return $this->response($voucher);
    }
}
