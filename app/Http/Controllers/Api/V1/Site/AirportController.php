<?php

namespace App\Http\Controllers\Api\V1\Site;

use App\Http\Controllers\Controller;
use App\Services\Api\V1\Site\AirportService;

class AirportController extends Controller
{
    private $service;

    public function __construct(AirportService $service)
    {
        $this->service = $service;
    }

    public function index()
    {
        $airports = $this->service->listAll();

        return $this->response($airports);
    }
}
