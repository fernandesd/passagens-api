<?php

namespace App\Http\Controllers\Api\V1\Site;

use App\Http\Controllers\Controller;
use App\Services\Api\V1\Site\BuyerService;

class BuyerController extends Controller
{
    private $service;

    public function __construct(BuyerService $service)
    {
        $this->service = $service;
    }

    public function getTickets($buyerCpf)
    {
        $tickets = $this->service->getTickets($buyerCpf);

        return $this->response($tickets);
    }
}
