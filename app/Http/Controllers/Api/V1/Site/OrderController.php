<?php

namespace App\Http\Controllers\Api\V1\Site;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\V1\Site\CreateOrderRequest;
use App\Services\Api\V1\Site\OrderService;

class OrderController extends Controller
{
    private $service;

    public function __construct(OrderService $service)
    {
        $this->service = $service;
    }

    public function store(CreateOrderRequest $request)
    {
        $tickets = $this->service->buy($request->validated());

        return $this->response($tickets);
    }

    public function cancel($orderCode)
    {
        $cancelled = $this->service->cancel($orderCode);

        return $this->response($cancelled);
    }
}
