<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\V1\Admin\FlightRequest;
use App\Services\Api\V1\Admin\FlightService;

class FlightController extends Controller
{
    private $service;

    public function __construct(FlightService $service)
    {
        $this->service = $service;
    }

    public function index()
    {
        $flights = $this->service->listAll();

        return $this->response($flights);
    }

    public function store(FlightRequest $request)
    {
        $flights = $this->service->store($request->validated());

        return $this->response($flights);
    }

    public function getPassengers($flightId)
    {
        $passengers = $this->service->getPassengers($flightId);

        return $this->response($passengers);
    }
}
