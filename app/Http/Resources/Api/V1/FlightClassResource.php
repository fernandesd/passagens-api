<?php

namespace App\Http\Resources\Api\V1;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class FlightClassResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        $data = [
            'type' => $this->type,
        ];

        $additionalData = [
            'capacity' => $this->capacity,
            'capacityUsed' => $this->passengers->count(),
            'capacityAvailable' => ($this->capacity - $this->passengers->count()),
            'passengers' => PassengerResource::collection($this->passengers),
        ];

        return $request->routeIs('api.admin.*') ? [...$data, ...$additionalData] : $data;
    }
}
