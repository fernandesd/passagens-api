<?php

namespace App\Http\Resources\Api\V1;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class TicketBaggageVoucherResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'passenger' => [
                'name' => $this->passenger->name,
                'CPF' => $this->passenger->cpf,
            ],
            'flight' => ['code' => $this->flight->code],
            'ticket' => ['code' => $this->code],
            'code' => $this->baggage->code,
        ];
    }
}
