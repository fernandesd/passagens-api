<?php

namespace App\Http\Resources\Api\V1;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class FlightResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'code' => $this->code,
            'status' => $this->status,
            'departAt' => $this->depart_at,
            'origin' => new AirportResource($this->origin),
            'destination' => new AirportResource($this->destination),
            'classes' => FlightClassResource::collection($this->classes),
        ];
    }
}
