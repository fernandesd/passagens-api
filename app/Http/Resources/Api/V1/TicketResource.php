<?php

namespace App\Http\Resources\Api\V1;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class TicketResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        $data = [
            'code' => $this->code,
            'hasBaggage' => (bool) $this->has_baggage,
            'price' => $this->total_price,
            'seat' => new SeatResource($this->seat),
            'baggageCode' => new BaggageResource($this->baggage),
        ];

        $relations = [
            'flightClass' => new FlightClassResource($this->flightClass),
            'passenger' => new PassengerResource($this->passenger),
            'flight' => new FlightResource($this->flight),
        ];

        return ! $request->routeIs('api.admin.*') ? [...$data, ...$relations] : $data;
    }
}
