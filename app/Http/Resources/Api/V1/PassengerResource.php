<?php

namespace App\Http\Resources\Api\V1;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class PassengerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        $data = [
            'name' => $this->name,
            'cpf' => $this->cpf,
            'email' => $this->email,
            'brithday' => $this->brithday,
        ];

        $relations = ['ticket' => new TicketResource($this->ticket)];

        return $request->routeIs('api.admin.*') ? [...$data, ...$relations] : $data;
    }
}
