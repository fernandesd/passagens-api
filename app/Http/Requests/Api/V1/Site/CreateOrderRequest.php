<?php

namespace App\Http\Requests\Api\V1\Site;

use Illuminate\Foundation\Http\FormRequest;

class CreateOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return auth()->guest();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'buyer' => 'required|array',
            'buyer.*' => 'required|string',
            'buyer.email' => 'email',
            'buyer.brithday' => 'date-format:Y-m-d',
            'passengers' => 'required|array',
            'passengersCount' => 'required|numeric',
            'passengers.*.*' => 'required',
            'passengers.*.email' => 'email',
            'passengers.*.brithday' => 'date-format:Y-m-d',
            'passengers.*.hasBaggage' => 'nullable|boolean',
            'flight' => 'required|array',
            'flight.*' => 'required|string',
            'flight.code' => 'exists:flights,code',
            'flight.classType' => 'exists:flight_classes,type',
        ];
    }

    /**
     * Prepare the data for validation.
     */
    protected function prepareForValidation(): void
    {
        $this->merge([
            'passengersCount' => count($this->passengers),
        ]);
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'date_format' => 'O campo :attribute não corresponde ao formato :format.',
            'exists' => 'O campo :attribute selecionado não existe no db.',
            'required' => 'O campo :attribute é obrigatório.',
            'array' => 'O campo :attribute deve ser um array.',
            'numeric' => 'O campo :attribute deve ser um número.',
            'email' => 'O campo :attribute deve ser um endereço de e-mail válido.',
        ];
    }
}
