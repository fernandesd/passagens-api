<?php

namespace App\Http\Requests\Api\V1\Admin;

use App\Models\Airport;
use Illuminate\Foundation\Http\FormRequest;

class FlightRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'depart_at' => 'required|date-format:Y-m-d H:i',
            'origin' => 'required|string|size:3|exists:airports,iata_code',
            'destination' => 'required|string|size:3|exists:airports,iata_code',
            'classes' => 'required|array',
            'classes.*.type' => 'required|string|in:economic,premium,business',
            'classes.*.seat_price' => 'required|integer',
            'classes.*.capacity' => 'required|integer',
        ];
    }

      /**
       * Get custom messages for validator errors.
       *
       * @return array
       */
      public function messages()
      {
          return [
              'date_format' => 'O campo :attribute não corresponde ao formato :format.',
              'exists' => 'O campo :attribute selecionado não existe no db.',
              'required' => 'O campo :attribute é obrigatório.',
              'size' => 'O campo :attribute deve conter :size caracteres',
              'array' => 'O campo :attribute deve ser um array.',
              'numeric' => 'O campo :attribute deve ser um número.',
              'in' => 'O campo :attribute deve ser economic,premium,business.',
              'integer' => 'O campo :attribute deve ser um número inteiro ex: R$100,00 deve ser passado 10000.',
              'email' => 'O campo :attribute deve ser um endereço de e-mail válido.',
          ];
      }

    /**
     * Prepare the data for validation.
     */
    protected function prepareForValidation(): void
    {
        $this->merge([
            'origin_airport_id' => Airport::where('iata_code', $this->origin)->first()->id,
            'destination_airport_id' => Airport::where('iata_code', $this->destination)->first()->id,
        ]);
    }
}
