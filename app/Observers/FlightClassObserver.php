<?php

namespace App\Observers;

use App\Models\FlightClass;

class FlightClassObserver
{
    /**
     * Handle the FlightClass "created" event.
     */
    public function created(FlightClass $flightClass): void
    {
        $ticketsData = [];
        for ($i = 0; $i < $flightClass->capacity; $i++) {
            $ticketsData[] = [
                'code' => uniqid(),
                'total_price' => $flightClass->seat_price,
                'flight_id' => $flightClass->flight_id,
            ];
        }

        $tickets = $flightClass->tickets()->createMany($ticketsData);

        $tickets->map(function ($ticket, $index) {
            $ticket->seat()->create(['number' => ++$index, 'flight_class_id' => $ticket->flight_class_id]);
        });
    }

    /**
     * Handle the FlightClass "updated" event.
     */
    public function updated(FlightClass $flightClass): void
    {
        //
    }

    /**
     * Handle the FlightClass "deleted" event.
     */
    public function deleted(FlightClass $flightClass): void
    {
        //
    }

    /**
     * Handle the FlightClass "restored" event.
     */
    public function restored(FlightClass $flightClass): void
    {
        //
    }

    /**
     * Handle the FlightClass "force deleted" event.
     */
    public function forceDeleted(FlightClass $flightClass): void
    {
        //
    }
}
