<?php

namespace App\Connector;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Client\Pool;
use Illuminate\Http\Client\Response;
use Illuminate\Support\Facades\Http;

class IBGEConnector
{
    public $baseUrl;

    public function __construct()
    {
        $this->baseUrl = config('connector.ibge.baseUrl');
    }

    /**
     * Envia uma requisição GET síncrona
     *
     * @param  string  $endpoint - endpoint que será concatenado a baseUrl
     * @return Response - retorno da requisição
     */
    public function getSync(string $endpoint): Response
    {
        return Http::get("{$this->baseUrl}/{$endpoint}");
    }

    /**
     * Busca as cidades vinculadas aos estados utilizando requisições HTTP concorrentes
     *
     * @param  Collection  $states - Array de objetos com estados brasileiros salvos no banco de dados
     * @return array<Response> - Array de objetos Response
     */
    public function getStateCitiesPool(Collection $states): array
    {
        return Http::pool(
            fn (Pool $client) => $states->map(function ($state) use ($client) {
                return [
                    $client->get("{$this->baseUrl}/localidades/estados/{$state->code}/municipios"),
                ];
            })
        );
    }
}
