<?php

namespace App\Repository\Api\V1;

use App\Models\Airport;
use Illuminate\Support\Collection;

class AirportRepository
{
    /**
     * Lista os aeroportos
     */
    public function getAll(): Collection
    {
        $airports = Airport::with('city')->orderBy('name')->paginate(100);

        return $airports;
    }
}
