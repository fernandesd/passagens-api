<?php

namespace App\Repository\Api\V1\Site;

use App\Models\Buyer;
use Illuminate\Support\Collection;

class BuyerRepository
{
    /**
     * Busca as passagens pelo cpf do comprador
     *
     * @param $buyercpf - cpf do comprador
     */
    public function getTickets(string $modelCpf): Collection
    {
        $model = Buyer::where('cpf', $modelCpf)->with(
            [
                'tickets' => [
                    'seat', 'baggage', 'passenger', 'flight' => ['origin.city', 'destination.city'],
                ],
            ],
        )->first();

        return $model->tickets;
    }

    public function new(array $buyerData)
    {
        return Buyer::updateOrCreate(collect($buyerData)->only('cpf', 'email')->toArray(), $buyerData);
    }
}
