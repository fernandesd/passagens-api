<?php

namespace App\Repository\Api\V1\Site;

use App\Models\Airport;

class AirportRepository
{
    public function getAll()
    {
        $airports = Airport::with('city')->orderBy('name')->paginate(100);

        return $airports;
    }
}
