<?php

namespace App\Repository\Api\V1\Site;

use App\Models\Order;

class OrderRepository
{
    /**
     * Busca pedido pelo código
     */
    public function findByCode(string $orderCode): Order
    {
        return Order::where('code', $orderCode)->firstOrFail();
    }

    public function updateToCancelOrder(Order $order): Order
    {
        $order->update(['status' => 'cancelled']);
        $ticketsBuilder = $order->tickets();
        $ticketsBuilder->update(['passenger_id' => null, 'buyer_id' => null, 'has_baggage' => false]);
        $ticketsBuilder->get('id')->each(fn ($ticket) => $ticket->baggage()->delete());

        return $order->refresh()->with([
            'buyer',
            'tickets' => [
                'seat', 'flight' => [
                    'origin.city', 'destination.city',
                ],
            ],
        ])->first();
    }
}
