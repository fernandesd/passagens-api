<?php

namespace App\Repository\Api\V1\Site;

use App\Models\Flight;

class FlightRepository
{
    public function getAll(array $data)
    {
        $tickets = Flight::with([
            'origin.city', 'destination.city',
        ]);

        return $tickets->get();
    }
}
