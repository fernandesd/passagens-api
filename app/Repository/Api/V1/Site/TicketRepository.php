<?php

namespace App\Repository\Api\V1\Site;

use App\Models\Buyer;
use App\Models\Flight;
use App\Models\Order;
use App\Models\Passenger;
use App\Models\Ticket;
use Illuminate\Support\Collection;

class TicketRepository
{
    /**
     * Lista as passagens disponíveis
     */
    public function getAll(): Collection
    {
        $tickets = Ticket::availableToBuy()->with([
            'seat', 'flight' => [
                'origin.city', 'destination.city',
            ],
        ])
            ->orderBy('flight_id')
            ->orderBy('flight_class_id');

        return $tickets->get();
    }

    /**
     * Busca uma passagem pelo código e retorna as relações de voo, passageiro e bagagem
     */
    public function getTicketWithRelationsByCode($ticketCode)
    {
        return Ticket::whereHas('baggage')->where('code', $ticketCode)->with([
            'seat', 'flight' => [
                'origin.city', 'destination.city',
            ],
        ])->firstOrFail();
    }

    /**
     * Busca uma passagens pelos dados de voo e retorna as relações de voo, passageiro e bagagem
     */
    public function getTicketsWithRelationsByFlightData($flightData)
    {
        $tickets = Ticket::availableToBuy()
            ->whereRelation('flight', 'code', '=', $flightData['code'])
            ->whereRelation('flightClass', 'type', '=', $flightData['classType'])
            ->with([
                'seat', 'flight' => [
                    'origin.city', 'destination.city',
                ],
            ]);

        return $tickets->get();
    }

    /**
     *  Filtra as passagens por data[yyyy-mm-dd], voo de origem[IATA] e voo de destino[IATA]
     *
     *  @param  array  $data - dados para a filtragem
     *  @return Illuminate\Support\Collection lista com as passagens encontradas
     */
    public function filterFlightTicketsByData(array $data): Collection
    {
        $filteredTickets = Flight::doesntHave('tickets.buyer')->whereDate('depart_at', '=', $data['departAt'])
            ->whereRelation('origin', 'iata_code', 'like', $data['flightOrigin'] ?? '%')
            ->whereRelation('destination', 'iata_code', 'like', $data['flightDestination'] ?? '%')
            ->with(['tickets' => [
                'seat', 'flight' => [
                    'origin.city', 'destination.city',
                ],
            ]]);

        return $filteredTickets->get()->pluck('tickets')->flatten()
            ->sortBy('flight_id')
            ->sortBy('flight_class_id')
            ->sortBy('price');
    }

    /**
     * Vincula o comprador às passagens recebidas
     */
    public function attachBuyerIdOnSelectedTickets(Buyer $buyer, array $ticketIds): bool
    {
        return Ticket::whereIn('id', $ticketIds)->update(['buyer_id' => $buyer->id]);
    }

    /**
     *  Cria o passageiro e vincula a passagem
     */
    public function attachPassengerIdOnSelectedTickets(array $passengerData, int $ticketId): Ticket
    {
        $ticket = Ticket::find($ticketId);
        $passengerData['flight_class_id'] = $ticket->flight_class_id;

        $passenger = Passenger::updateOrCreate(collect($passengerData)->only(['cpf', 'email'])->toArray(), $passengerData);

        $ticketDataToUpdate = ['passenger_id' => $passenger->id];

        if (isset($passengerData['hasBaggage'])) {
            $ticketNewPrice = $ticket->total_price += ($ticket->total_price * 0.1);
            $ticketDataToUpdate = [...$ticketDataToUpdate, ...['total_price' => $ticketNewPrice, 'has_baggage' => true]];
            $ticket->baggage()->create();
        }

        $ticket->update($ticketDataToUpdate);

        return $ticket;
    }

    /**
     * Cria o pedido e vincula o comprador e as passagens
     *
     * @return Order Instância do pedido realizado e sua relações
     */
    public function attachOrderIdOnSelectedTickets(Buyer $buyer, array $ticketIds): Order
    {
        $tickets = Ticket::whereIn('id', $ticketIds);
        $ticketsSum = $tickets->sum('total_price');

        $order = $buyer->orders()->create(['total' => $ticketsSum, 'code' => uniqid()]);

        Ticket::whereIn('id', $ticketIds)->update(['buyer_id' => $buyer->id, 'order_id' => $order->id]);

        $updatedOrder = Order::where('id', $order->id)->with([
            'buyer',
            'tickets' => [
                'seat', 'flight' => [
                    'origin.city', 'destination.city',
                ],
            ],
        ]);

        return $updatedOrder->first();
    }
}
