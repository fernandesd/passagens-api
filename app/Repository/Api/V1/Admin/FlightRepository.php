<?php

namespace App\Repository\Api\V1\Admin;

use App\Models\Airport;
use App\Models\Flight;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

class FlightRepository
{
    /**
     * Lista os voos
     */
    public function getAll(): LengthAwarePaginator
    {
        $flights = Flight::with([
            'origin.city', 'destination.city',
            'classes' => [
                'passengers' => [
                    'ticket' => [
                        'seat', 'baggage',
                    ],
                ],
            ],
        ]);

        return $flights->paginate(100);
    }

    /**
     * Lista os passageiros de um voo
     */
    public function getPassengers(string $modelId): Collection
    {
        $model = Flight::where('id', $modelId)->with([
            'classes' => [
                'passengers' => [
                    'ticket' => [
                        'seat', 'baggage',
                    ],
                ],
            ],
        ])->first();

        return $model->classes;
    }

    /**
     * Cria um novo voo
     */
    public function createFlight(array $flightData): Flight
    {
        $flight = Flight::create($flightData);

        $flight->classes()->createMany($flightData['classes']);

        return Flight::with(['classes' => ['seats', 'tickets']])->find($flight->id);
    }

    public function getFlightAirports(array $iatas): Collection
    {
        return Airport::with('city')->whereIn('iata_code', $iatas)->get()->unique('city.id');
    }
}
