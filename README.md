# DebugAir - Uma nova descoberta a cada vôo
Este é um teste aplicado para testar os conhecimentos de Laravel.
### Principais tecnologias utilizadas
 - PHP 8.2
 - Composer 2
 - Laravel 10
 - Mysql 8.0
 - Docker 20.10.22
 - Git
### Como instalar (necessário ter docker instalado):
 1. Clonar o projeto
 2. `cd debugAir-api`
 3. `./.scripts/dev-install.sh`
 4. `docker compose exec laravel php artisan migrate --seed` 
 6. A aplicação estará disponível em: [http://localhost](http://localhost)
 - Executando comandos: `docker compose exec laravel php artisan tinker`

### Usuário admin
- login: admin@admin.com
- senha: password
